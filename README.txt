Display popup messages for users once per browser session.

Configuration:
----------------------
Go to admin/settings/popup_messages and set message title and body.
Go to admin/user/permissions and set permissions.

Authors:
----------------------
Drupal module author: August Yip augustyip@gmail.com
jQuery and CSS code: Adrian "yEnS" Mato Gondelle yensamg@gmail.com, Twitter: @adrianmg
