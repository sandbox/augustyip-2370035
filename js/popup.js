/**
 * @file
 * jQuery code.
 * Based on code: Adrian "yEnS" Mato Gondelle, twitter: @adrianmg
 * Modifications for Drupal: August Yip augustyip@gmail.com
 */

(function ($) {
  // Setting up popup.
  // 0 means disabled; 1 means enabled.
  var popupStatus = 0;

  /**
   * Loading popup with jQuery.
   */
  function popup_message_load_popup() {
    // Loads popup only if it is disabled.
    if (popupStatus == 0) {
      jQuery("#popup-message-background").css( {
        "opacity": "0.7"
      });
      jQuery("#popup-message-background").fadeIn("slow");
      jQuery("#popup-message-window").fadeIn("slow");
      popupStatus = 1;
    }
  }

  /**
   * Disabling popup with jQuery.
   */
  function popup_message_disable_popup() {
    // Disables popup only if it is enabled.
    if (popupStatus == 1) {
      jQuery("#popup-message-background").fadeOut("slow");
      jQuery("#popup-message-window").fadeOut("slow");
      popupStatus = 0;

      jQuery('#popup-message-window').remove();
      jQuery('#popup-message-background').remove();
      var messages = Drupal.settings.popup_message.messages;
      if (!jQuery.isEmptyObject(messages)){
        popup_message_display_popup(messages);
      }
    }
  }

  /**
   * Centering popup.
   */
  function popup_message_center_popup(width, height) {
    // Request data for centering.
    var windowWidth = document.documentElement.clientWidth;
    var windowHeight = document.documentElement.clientHeight;

    var popupWidth = 0
    if (typeof width == "undefined") {
      popupWidth = $("#popup-message-window").width();
    }
    else {
      popupWidth = width;
    }
    var popupHeight = 0
    if (typeof width == "undefined") {
      popupHeight = $("#popup-message-window").height();
    }
    else {
      popupHeight = height;
    }

    // Centering.
    jQuery("#popup-message-window").css( {
      "position": "absolute",
      "width" : popupWidth + "px",
      "height" : popupHeight + "px",
      "top": windowHeight / 2 - popupHeight / 2,
      "left": windowWidth / 2 - popupWidth / 2
    });
    // Only need force for IE6.
    jQuery("#popup-message-background").css( {
      "height": windowHeight
    });

  }

  /**
   * Display popup message.
   */
  function popup_message_display_popup(messages) {

    if (jQuery.isEmptyObject(messages)) {
      return;
    }

    var limit = 0;
    var popup_message_title = '';
    var popup_message_body = '';

    jQuery.each(messages, function(key, value) {
       if (limit == 0) {
        popup_message_title = value.title;
        popup_message_body = value.body;
        popup_message_key = key;
        delete Drupal.settings.popup_message.messages[key];
        limit += 1;
       }
    });

    var width = Drupal.settings.popup_message.width;
    var height = Drupal.settings.popup_message.height;
    width = popup_message_get_last_object_item(width);
    height = popup_message_get_last_object_item(height);

    jQuery('body').append('<div id="popup-message-window" class="' + popup_message_key + '"><a id="popup-message-close">X</a>\n\
      <h1 class="popup-message-title">' + popup_message_title + '</h1><div id="popup-message-content">' + popup_message_body
      + '<a href="#" id="popup-message-remind-me-later" data-message_key="' + popup_message_key + '">Remind me later</a></div></div><div id="popup-message-background"></div>');

    // Loading popup.
    popup_message_center_popup(width, height);
    popup_message_load_popup();

    // Closing popup.
    // Click the x event!
    jQuery("#popup-message-close").click(function() {
      popup_message_disable_popup();
    });
    // Click out event!
    jQuery("#popup-message-background").click(function() {
      popup_message_disable_popup();
    });
    // Press Escape event!
    jQuery(document).keypress(function(e) {
      if (e.keyCode == 27 && popupStatus == 1) {
        popup_message_disable_popup();
      }
    });
  }

  /**
   * Helper function for get last element from object.
   * Used if on page is loaded more than one message.
   */
  function popup_message_get_last_object_item(variable_data) {
    if (typeof(variable_data) == 'object') {
        variable_data = variable_data[(variable_data.length - 1)];
    }
    return variable_data;
  }

  Drupal.behaviors.popup_message = {
    attach: function(context) {

      popup_message_display_popup(Drupal.settings.popup_message.messages);

      $('#popup-message-remind-me-later').live('click', function(event) {
        event.preventDefault();
        var message_key = $(this).attr('data-message_key');
        $.ajax({
          'url': Drupal.settings.basePath+'popup_message_remind_me_later/' + message_key
        });
        popup_message_disable_popup();
        return false;
      });
    }
  };
})(jQuery);
